package com.kozak.logging;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "ACd2a80237ae8888d3883a7c054c0e7616";
    public static final String AUTH_TOKEN = "1f594e20d5156a675ffd6e57cf6f5b1b";
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380987420461"), /*my phone number*/
                        new PhoneNumber("+15407017120"), str) .create(); /*attached to me number*/
    }
}